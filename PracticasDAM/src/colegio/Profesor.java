package colegio;

public class Profesor extends Persona {



	//private String nombre;
	//private String direccion;
	private String tlf;
	private String dni;
	private String asignatura;
	private String cargo;
	
	
	
	public Profesor(String nombre, String direccion, String tlf, String dni, String asignatura, String cargo) {
		super(nombre, direccion);
		//this.nombre = nombre;
		//this.direccion = direccion;
		this.tlf = tlf;
		this.dni = dni;
		this.asignatura = asignatura;
		this.cargo = cargo;
	}
	
	/*public Profesor(String nombre, String direccion, String tlf) {
		super(nombre, direccion);
		//this.nombre = nombre;
		//this.direccion = direccion;   ******SI INCLUYO ESTA CLASE NO SALE******
		this.tlf = tlf;
	}*/
	
	/*public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}*/

	public String getDni() {
		if(pruebaDNI() == true) {
			return dni;
		}else {
			return "No v�lido"; //mensaje de error
		}
	}
	public void setDni(String Dni) {
		dni = Dni;
	}
	public String getTlf() {
		return tlf;
	}
	public void setTlf(String tlf) {
		this.tlf = tlf;
	}
	public String getAsignatura() {
		return asignatura;
	}
	public void setAsignatura(String asignatura) {
		this.asignatura = asignatura;
	}
	public String getCargo() {
		return cargo;
	}
	public void setCargo(String cargo) {
		this.cargo = cargo;
	}
	
	
	@Override
	public String toString() {
		return "Profesor [" + super.toString() + ", tlf=" + tlf + ", DNI=" + getDni() + ", asignatura=" + asignatura
				+ ", cargo=" + cargo + "]";
	}
	
	public boolean pruebaDNI() {

		char letras [] = {'T', 'R', 'W', 'A', 'G', 'M', 'Y', 'F', 'P', 'D',
				'X', 'B', 'N', 'J', 'Z', 'S', 'Q', 'V', 'H', 'L', 'C', 'K', 'E'}; //Array de letras para comprobar si es correcto
		char letraDNI = (dni.toUpperCase().charAt(8)); //se pasa a mayuscula para que si el usuario mete en minuscula la letra se compruebe bien. despues el caracter en la posicion 8 que es donde se encuentra la letra
		int ndni = Integer.parseInt(dni.substring(0,dni.length()-1)); //se pasa a int cada valor de dni excepto la letra que no se mete
		
		
		if(letraDNI == letras[ndni % 23] && dni.length() == 9){ //la operacion de comprobacion y quedarse con el resto y la comprobacion de que tiene los caracteres requeridos
			return true;
		}else {
			return false;
		}
				
	}
}
