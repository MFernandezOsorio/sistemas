package colegio;

public class Estudiante extends Persona {



	//private String nombre;
	private String nia;
	//private String direccion;
	private String curso;
	private double notaMedia;
	
	
	
	public Estudiante(String nombre, String nia, String direccion, String curso, double notaMedia) {
		super(nombre, direccion);
		//this.nombre = nombre;
		this.nia = nia;
		//this.direccion = direccion;
		this.curso = curso;
		this.notaMedia = notaMedia;
	}
	
	/*public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}*/
	public String getNia() {
		return nia;
	}
	public void setNia(String nia) {
		this.nia = nia;
	}
	/*public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}*/

	public String getCurso() {
		return curso;
	}
	public void setCurso(String curso) {
		this.curso = curso;
	}
	public double getnotaMedia() {
		return notaMedia;
	}
	public void setnotaMedia(double notaMedia) {
		this.notaMedia = notaMedia;
	}
	
	@Override
	public String toString() {
		return "Estudiante [" + super.toString() + ", NIA =" + nia + ", curso=" + curso
				+ ", notaMedia=" + notaMedia + "]";
	}
}
