package colegio;

import java.util.Scanner;

public class Principal {

	public static void main(String[] args) {
		Scanner leerTeclado=new Scanner(System.in);
		
		
		//Profesor dos = new Profesor(null, null, null, null, null, null);
		
		//Estudiante a = new Estudiante("Ana", "001", "Avenida Piruleta", "1�", 8.5);
		//Estudiante b = new Estudiante("Jose", "007", "Evergreen Terrace", "5�", 4.5);
		
		//PAS pas1 = new PAS("Jorge", "El muelle", "852369", "741258", "Admin");
		//PAS pas2 = new PAS("Silvia", "De San Blas", "200220", "123789", "Conserje");
		
		System.out.println("Programa de introducci�n de datos");

		boolean exit = false;
		do {
			try {
				Profesor (leerTeclado);//declaracion de funciones de recogida de datos
				Estudiante (leerTeclado);
				PAS (leerTeclado);
				System.out.println("Si desea volver a introducir los datos pulse 1, 2 para salir");
				int opcion = leerTeclado.nextInt();
				switch (opcion) {
				case 1:
					System.out.println("Programa de introducci�n de datos");
					break;
				case 2:
					System.out.println("Adi�s");
					exit = true;
					break;
				default:
					System.out.println("Recuerde, si desea volver a introducir los datos pulse 1, 2 para salir");
				}
				}catch(Exception e){//c�digo de error
					System.out.println("Por favor, introduce los datos correctamente.\nNota: DNI: 8 numeros 1 letra.\n Nota media: 0,0");
				}			
		}while(exit==false);//para salir
		leerTeclado.close();

		//System.out.println( "Profesores:\n\t" + /*uno  +*/ " \n\t" + profesor.toString());
		
		//System.out.println( "Estudiantes:\n\t" + a.toString() + " \n\t" + b.toString());
		
		//System.out.println( "PAS:\n\t" + pas1.toString() + " \n\t" + pas2.toString());
	}
	
	//de aqu� hacia debajo la recogida de datos al usuario
	
	public static void Profesor(Scanner leerTeclado) {
		Profesor profesor = new Profesor(null, null, null, null, null, null);
		Profesor profesor2 = new Profesor(null, null, null, null, null, null);
		
		System.out.println("Por favor, introduzca el nombre del profesor 1: ");
		String n1 = leerTeclado.nextLine();
		profesor.setNombre(n1); //con esto le indicas donde debe rellenar este campo 
		
		System.out.println("Por favor, introduzca el nombre del profesor 2: ");
		String n2 = leerTeclado.nextLine();
		profesor2.setNombre(n2);
		
		System.out.println("Por favor, introduzca el direcci�n del profesor 1: ");
		String d1 = leerTeclado.nextLine();
		profesor.setDireccion(d1);
		
		System.out.println("Por favor, introduzca el direcci�n del profesor 2: ");
		String d2 = leerTeclado.nextLine();
		profesor2.setDireccion(d2);
		
		System.out.println("Por favor, introduzca el tel�fono del profesor 1: ");
		String t1 = leerTeclado.nextLine();
		profesor.setTlf(t1);
		
		System.out.println("Por favor, introduzca el tel�fono del profesor 2: ");
		String t2 = leerTeclado.nextLine();
		profesor2.setTlf(t2);
		
		System.out.println("Por favor, introduzca el DNI del profesor 1: ");
		String dni1 = leerTeclado.nextLine();
		profesor.setDni(dni1);
		
		System.out.println("Por favor, introduzca el DNI del profesor 2: ");
		String dni2 = leerTeclado.nextLine();
		profesor2.setDni(dni2);
		
		System.out.println("Por favor, introduzca la asignatura del profesor 1: ");
		String a1 = leerTeclado.nextLine();
		profesor.setAsignatura(a1);
		
		System.out.println("Por favor, introduzca la asignatura del profesor 2: ");
		String a2 = leerTeclado.nextLine();
		profesor2.setAsignatura(a2);
		
		System.out.println("Por favor, introduzca el cargo del profesor 1: ");
		String c1 = leerTeclado.nextLine();
		profesor.setCargo(c1);
		
		System.out.println("Por favor, introduzca el cargo del profesor 2: ");
		String c2 = leerTeclado.nextLine();
		profesor2.setCargo(c2);
		
		//Profesor uno = new Profesor(n1, d1, t1, dni1, a1, c1);
		//Profesor dos = new Profesor(n2, d2, t1, dni2, a2, c2);
		System.out.println( "Profesores:\n\t" + profesor.toString() + " \n\t" + profesor2.toString());

	}
	
	public static void Estudiante(Scanner leerTeclado) {
		Estudiante a = new Estudiante(null, null, null, null, 0.0);
		Estudiante b = new Estudiante(null, null, null, null, 0.0);
		
		System.out.println("Por favor, introduzca el nombre del estudiante 1: ");
		String n1 = leerTeclado.nextLine();
		a.setNombre(n1);
		
		System.out.println("Por favor, introduzca el nombre del estudiante 2: ");
		String n2 = leerTeclado.nextLine();
		b.setNombre(n2);
		
		System.out.println("Por favor, introduzca el NIA del estudiante 1: ");
		String nia1 = leerTeclado.nextLine();
		a.setNia(nia1);
		
		System.out.println("Por favor, introduzca el NIA del estudiante 2: ");
		String nia2 = leerTeclado.nextLine();
		b.setNia(nia2);
		
		System.out.println("Por favor, introduzca el direcci�n del estudiante 1: ");
		String d1 = leerTeclado.nextLine();
		a.setDireccion(d1);
		
		System.out.println("Por favor, introduzca el direcci�n del estudiante 2: ");
		String d2 = leerTeclado.nextLine();
		b.setDireccion(d2);
		
		System.out.println("Por favor, introduzca el curso del estudiante 1: ");
		String c1 = leerTeclado.nextLine();
		a.setCurso(c1);
		
		System.out.println("Por favor, introduzca el curso del estudiante 2: ");
		String c2 = leerTeclado.nextLine();
		b.setCurso(c2);
		
		System.out.println("Por favor, introduzca la nota media del estudiante 1: ");
		double nm1 = leerTeclado.nextDouble(); //atenciaon es de tipo double
		a.setnotaMedia(nm1);
		
		System.out.println("Por favor, introduzca la nota media del estudiante 2: ");
		double nm2 = leerTeclado.nextDouble();
		b.setnotaMedia(nm2);
		
		
		//Profesor uno = new Profesor(n1, d1, t1, dni1, a1, c1);
		//Profesor dos = new Profesor(n2, d2, t1, dni2, a2, c2);
		System.out.println( "Estudiantes:\n\t" + a.toString() + " \n\t" + b.toString());

	}
	
	public static void PAS(Scanner leerTeclado) {
		PAS pas1 = new PAS(null, null, null, null, null);
		PAS pas2 = new PAS(null, null, null, null, null);
		
		System.out.println("\nPor favor, introduzca el nombre del PAS 1: ");
		String n1 = leerTeclado.nextLine();
		pas1.setNombre(n1);
		
		System.out.println("Por favor, introduzca el nombre del PAS 2: ");
		String n2 = leerTeclado.nextLine();
		pas2.setNombre(n2);
		
		System.out.println("Por favor, introduzca el direcci�n del PAS 1: ");
		String d1 = leerTeclado.nextLine();
		pas1.setDireccion(d1);
		
		System.out.println("Por favor, introduzca el direcci�n del PAS 2: ");
		String d2 = leerTeclado.nextLine();
		pas2.setDireccion(d2);
		
		System.out.println("Por favor, introduzca el tel�fono del PAS 1: ");
		String t1 = leerTeclado.nextLine();
		pas1.setTlf(t1);
		
		System.out.println("Por favor, introduzca el tel�fono del PAS 2: ");
		String t2 = leerTeclado.nextLine();
		pas2.setTlf(t2);
		
		System.out.println("Por favor, introduzca el DNI del PAS 1: ");
		String dni1 = leerTeclado.nextLine();
		pas1.setDni(dni1);
		
		System.out.println("Por favor, introduzca el DNI del PAS 2: ");
		String dni2 = leerTeclado.nextLine();
		pas2.setDni(dni2);
		
		System.out.println("Por favor, introduzca el puesto del PAS 1: ");
		String p1 = leerTeclado.nextLine();
		pas1.setPuesto(p1);
		
		System.out.println("Por favor, introduzca el puesto del PAS 2: ");
		String p2 = leerTeclado.nextLine();
		pas2.setPuesto(p2);
		
		
		System.out.println( "PAS:\n\t" + pas1.toString() + " \n\t" + pas2.toString());

	}
}
