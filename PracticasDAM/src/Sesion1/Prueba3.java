package Sesion1;

import java.util.Scanner;
import java.math.*;

public class Prueba3 {

	public static void main(String[] args) {
		
		String nameuser;
		//int result1, result2;
		int stop = 0;
		double result1, result2;
		double a = 0, b = 0, c = 0;
		Scanner text = new Scanner(System.in);
		
		//Bienvenida al usuario.
		System.out.println("Hola. Soy Marta, una calculadora de ecuaciones de 2� grado.");
		System.out.println("�C�mo se llama usted?");
		nameuser=text.nextLine();
		System.out.println("�Bienvenido, " + nameuser + "!");
		System.out.println("Vamos a comenzar pidiendo los datos que desea introducir.");
		
		//Recogida de datos de ecuaci�n.
		while(stop != 1) {
			System.out.println("Teniendo en cuenta que una ecuaci�n de 2� grado tiene esta forma:");
			System.out.println("--> y = ax� + bx + c");
		
			System.out.println("�Qu� valor desea que obtenga a?");
			a=text.nextDouble();
			
			System.out.println("�Qu� valor desea que obtenga b?");
			b=text.nextDouble();
			
			System.out.println("�Qu� valor desea que obtenga c?");
			c=text.nextDouble();
			System.out.println("�Es esta la ecuaci�n que desea?\n" 
					+ "	--> y = " + a + "x� + " + b + "x +" + c
					+ "\n(Pulse 1 si desea continuar u otro n�mero si desea volver a introducir los datos).");
			System.out.println("--> y = " + a + "x� + " + b + "x +" + c);
			stop=text.nextInt();			
		}
		
		//Resultado final
		System.out.println("En la siguiente l�nea podr� leer la f�rmula de la ecuaci�n de 2� grado:");
		System.out.println("--> [(-b)+-(ra�z(b�)-(4*a*c)]/[(2*a)]");
		System.out.println("El resultado es igual a:");
		
		//Operaciones
		if ((b*b)-(4*a*c)<0)
			System.out.println("La ecuaci�n no tiene soluci�n");
			else { 
				result1 = (-b + Math.sqrt((b*b)-(4*a*c)))/(2*a);
				result2 = (-b - Math.sqrt((b*b)-(4*a*c)))/(2*a);
				System.out.println("--> a) " + result1);
				System.out.println("--> b) " + result2);
			}
		
		text.close();
	}

}
