package Sesion1;

public class Prueba2 {

    public static void main(String[] args) {
        String fragmentoNombreAlumnos = "Victor"; 
        String[] nombreAlumnos = {"Antonio", "Marta", "Victor Hugo", "David"}; 

    int resultado = contarUsuarios(fragmentoNombreAlumnos, nombreAlumnos);
    System.out.println("Total resultados: " + resultado); 
    }

    static int contarUsuarios(String fragmentoNombreAlumnos, String[] nombreAlumnos) {
       
    	int totalEncontrados = 0; 

        for (String nombreAlumnoActual : nombreAlumnos) { 
        	if (nombreAlumnoActual.contains(fragmentoNombreAlumnos)) {
                totalEncontrados++;
            }
        }
    return totalEncontrados;
    }	
 }
