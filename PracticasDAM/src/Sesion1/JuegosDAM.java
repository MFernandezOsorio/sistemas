package Sesion1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;


public class JuegosDAM {
	public static void main(String[] args) {
		Scanner leerTeclado=new Scanner(System.in);
		
		System.out.println("Bienvenido a juegos DAM");
		System.out.println("Existen 2 juegos, elige uno:\n 1-> Juego 1. \n 2-> Juego 2. \n 3-> Salir. \n");
		int opcion1=leerTeclado.nextInt();  //lee teclado
		boolean exit= false;
		
		do {
			switch (opcion1) { //primer switch para elegir el juego 
			case 1:
				System.out.println("Vamos a comenzar el juego de dibujar figuras\n");
				System.out.println("�Qu� quiere dibujar?\n 1-> Cuadrado. \n 2-> Tri�ngulo. \n 3-> Rombo. \n 4-> Salir. \n");
				int opcion2=leerTeclado.nextInt();
				do {					
					switch (opcion2) { //segundo switch dentro de juego 1 para elegir que pintar
					case 1:
						System.out.println("Vamos a dibujar un cuadrado.\n Por favor introduzca los siguientes datos: \n");
						System.out.println("�Cu�nto desea que mida el lado?\n");
						int l=leerTeclado.nextInt();
						cuadrado(l);
						break;
					case 2:
						System.out.println("Vamos a dibujar un tri�ngulo rect�ngulo.\n Por favor introduzca los siguientes datos: \n");
						System.out.println("�Cu�nto desea que mida la base?\n");
						int b=leerTeclado.nextInt();
						triangulo(b);
						break;
					case 3:
						System.out.println("Vamos a dibujar un rombo.\n Por favor introduzca los siguientes datos: \n");
						System.out.println("�Cu�nto desea que mida la diagonal?\n");
						int d=leerTeclado.nextInt();
						rombo(d);
						break;
					case 4:
						System.out.println("*��Gracias por jugar!!* \n Hasta la pr�xima, espero que se haya divertido conmigo.");
						exit = true;
						break;
					default:
						System.out.println("Recuerde: las opciones son: 1->Cuadrado. \n 2->Tri�ngulo. \n 3-> Rombo. \n 4-> Salir. \n");
					}
					if(!exit) {
						System.out.println("�Desea seguir jugando? \n Las opciones son: 1->Cuadrado. \n 2->Tri�ngulo. \n 3-> Rombo. \n 4-> Salir. \n");
						opcion2=leerTeclado.nextInt();
					}
				}while (exit==false);
				leerTeclado.close();
				break;
			case 2:
				System.out.println("Vamos a comenzar el juego de ordenar consonantes y vocales.\n Por favor introduzca los siguientes datos: \n");
				System.out.println("Escriba la palabra o frase de donde desea sacar la informaci�n: \n");
				String frase=leerTeclado.nextLine();
				palabra(frase);
				break;
			case 3:
				System.out.println("*��Gracias por jugar!!* \n Hasta la pr�xima, espero que se haya divertido conmigo.");
				exit = true;
				break;
			default:
				System.out.println("Error: las opciones son: \n 1-> Juego 1. \n 2-> Juego 2. \n 3-> Salir. \n"); //si no mete los datos requeridos salta esto
			}
			if(!exit) {
				System.out.println("�Desea seguir jugando? \n Las opciones son: \n 1-> Juego 1. \n 2-> Juego 2. \n 3-> Salir. \n"); //para cuando termine de jugar
				opcion1=leerTeclado.nextInt();
			}
		}while (exit==false); //se acaba el switch cun�ando exit sea false
		leerTeclado.close(); //se cierra la variable de scanner
	}
	
	//Se declaran funciones a utilizar dentro del main
	private static void cuadrado(int l) {		
		for (int i = 0; i < l; i++)//algoritmo cuadrado
			System.out.print("* ");
		
		System.out.println();
		
		for (int i = 0; i < (l - 2); i++) {
			System.out.print("*");
			for (int j = l * 2 - 3; j > 0; j--)
				System.out.print(" ");
			System.out.println("*");
		}
		for (int i = 0; i < l; i++)
			System.out.print("* ");		
	}
	
	private static void triangulo(int b) {		
		
		if (b % 2 == 1 && b > 0) {//Algoritmo rombo
			for (int i = 0; i < b; i++) {
				for (int j = 0; j <= i; j++)			
					if (j == 0 || i == (b - 1) || i == j)
						System.out.print("*");
					else
						System.out.print(" ");
				System.out.println();
			}
		} else {
			System.out.println("La base de la pir�mide tiene que ser un n�mero impar.");
		}
	}
	
	private static void rombo(int d) {		
		if (d % 2 == 1 && d > 0) { //Algoritmo rombo
			for (int i = 0; i < (d + 1) / 2; i++) {
				for (int j = 0; j < (d + 1) / 2 - i - 1; j++)
					System.out.print(" ");					
				for (int y = 0; y < 2 * i + 1; y++)
					System.out.print("*");				
				System.out.println();
			}
			for (int i = (d - 3) / 2; i >= 0 ; i--) {
				for (int y = 0; y < (d + 1) / 2 - i - 1; y++)
					System.out.print(" ");				
				for (int y = 0; y < 2 * i + 1; y++)
					System.out.print("*");
				System.out.println();
			}
		} else {
			System.out.println("La base del rombo tiene que ser un n�mero impar.");
		}
	}	
	
	private static void palabra(String frase) {	
		
		Scanner leerTeclado = new Scanner(System.in); //Algoritmo palabra
		frase = leerTeclado.nextLine();
		System.out.println("\nSu frase es: " + frase);		
		//Declaraci�n de arrays de tipo caracter
		ArrayList<Character> vocal = new ArrayList<Character>();
		ArrayList<Character> consonante = new ArrayList<Character>();
		
		for (int i = 0; i < frase.length() - 1; i++) { //primero comprueba si son vocales y las almacena en su array correspondiente
			char letra = frase.toLowerCase().charAt(i); 	 //toLowerCase convierte en minuscula, chartArt(i) caracter en esa posicion		
			if (letra == 'a' || letra == 'e' || letra == 'i' || letra == 'o' || letra == 'u') {
				vocal.add(frase.charAt(i)); //a�ade
				
			}
			if (letra != 'a' && letra != 'e' && letra != 'i' && letra != 'o' && letra != 'u' && letra != ' ') {				
				consonante.add(frase.charAt(i));
			}
		}
		System.out.println("Vocales: " + vocal.toString());
		Collections.sort(vocal); //ordena alfab�ticamente
		System.out.println("Vocales ordenadas: " + vocal.toString());
		System.out.println("Consonantes: " + consonante.toString());
		Collections.sort(consonante);
		System.out.println("Consonantes ordenadas: " + vocal.toString());
	}
}
