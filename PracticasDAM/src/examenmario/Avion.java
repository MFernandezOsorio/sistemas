package examenmario;

public class Avion extends Vehiculo {

	private byte motores;
	private double velocidad;
	private boolean combate;
	
	public byte getMotores() {
		return motores;
	}
	public void setMotores(byte motores) {
		this.motores = motores;
	}
	public double getVelocidad() {
		return velocidad;
	}
	public void setVelocidad(double velocidad) {
		this.velocidad = velocidad;
	}
	public boolean isCombate() {
		return combate;
	}
	public void setCombate(boolean combate) {
		this.combate = combate;
	}
	
	public Avion(String marca, String color, String numeroBastidor, int kilometros, int aņofabricacion, byte motores,
			double velocidad, boolean combate) {
		super(marca, color, numeroBastidor, kilometros, aņofabricacion);
		this.motores = motores;
		this.velocidad = velocidad;
		this.combate = combate;
	}
	
	public Avion(String marca, String color, String numeroBastidor, int kilometros, int aņofabricacion,
			boolean combate) {
		super(marca, color, numeroBastidor, kilometros, aņofabricacion);
		this.combate = combate;
		motores = 1;
		velocidad = 1.0;
	}
	
	@Override
	public String toString() {
		return "Avion [motores=" + motores + ", velocidad=" + velocidad + ", combate=" + combate + "]";
	}


	
}
