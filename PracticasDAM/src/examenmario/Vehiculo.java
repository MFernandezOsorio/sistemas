package examenmario;

public class Vehiculo {


	public String marca;
	public String color;
	public String numeroBastidor;
	public int kilometros;
	protected int aniofabricacion;
	



	public Vehiculo(String marca, String color, String numeroBastidor, int kilometros, int aņofabricacion) {
		//super();
		this.marca = marca;
		this.color = color;
		this.numeroBastidor = numeroBastidor;
		this.kilometros = kilometros;
		this.aniofabricacion = aņofabricacion;
	}
	
	
	public String getMarca() {
		return marca;
	}
	public void setMarca(String marca) {
		this.marca = marca;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public String getNumeroBastidor() {
		return numeroBastidor;
	}
	public void setNumeroBastidor(String numeroBastidor) {
		this.numeroBastidor = numeroBastidor;
	}
	public int getKilometros() {
		return kilometros;
	}
	public void setKilometros(int kilometros) {
		this.kilometros = kilometros;
	}
	public int getAņofabricacion() {
		return aniofabricacion;
	}
	public void setAņofabricacion(int aņofabricacion) {
		this.aniofabricacion = aņofabricacion;
	}


	@Override
	public String toString() {
		return "marca=" + marca + ", color=" + color + ", numeroBastidor=" + numeroBastidor + ", kilometros="
				+ kilometros + ", aņofabricacion=" + aniofabricacion + ", ";
	}
	


}
