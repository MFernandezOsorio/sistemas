package examenmario;

import java.util.Scanner;

import examenmario.Barco.Tipo;

	public class Principal {
	
		public static void main(String[] args) {
			Scanner leerTeclado=new Scanner(System.in);
			System.out.println("Examen Mario Fernández");
			
			Coche(leerTeclado);
			
	}
	
	public static void Coche (Scanner leerTeclado){
		Coche coche = new Coche(null, null, null, 0, 0, false, false);
		
		//String marca, String color, String numeroBastidor, int kilometros, int añofabricacion, boolean electrico
		
		System.out.println("Vamos a introducir los datos del coche. \n");
		
		System.out.println("Por favor, introduzca la marca: ");
		String m = leerTeclado.nextLine();
		coche.setMarca(m); //con esto le indicas donde debe rellenar este campo 
		
		System.out.println("Por favor, introduzca el color: ");
		String c = leerTeclado.nextLine();
		coche.setColor(c);
		
		System.out.println("Por favor, introduzca el número de bastidor: ");
		String nb = leerTeclado.nextLine();
		coche.setNumeroBastidor(nb);
		
		System.out.println("Por favor, introduzca los kilómetros: ");
		int km = leerTeclado.nextInt();
		coche.setKilometros(km);
		
		System.out.println("Por favor, introduzca el año de fabricación: ");
		int af = leerTeclado.nextInt();
		coche.setAñofabricacion(af);
		
		System.out.println("Por favor, introduzca true si es electrico y false si no lo es: ");
		boolean e = leerTeclado.nextBoolean();
		coche.setElectrico(e);
		
		System.out.println( "Vehículo:\n\t" + coche.toString() + " \n");

		
	}
	
	/*public static void Avion (Scanner leerTeclado){
		
		//String marca, String color, String numeroBastidor, int kilometros, int añofabricacion, int eslora,
		//int calado, Tipo tipo
		Avion avion = new Avion(null, null, null, 0, 0, 0, 0, false);
		
		//String marca, String color, String numeroBastidor, int kilometros, int añofabricacion, boolean electrico
		
		System.out.println("Vamos a introducir los datos del coche. \n");
		
		System.out.println("Por favor, introduzca la marca: ");
		String m = leerTeclado.nextLine();
		coche.setMarca(m); //con esto le indicas donde debe rellenar este campo 
		
		System.out.println("Por favor, introduzca el color: ");
		String c = leerTeclado.nextLine();
		coche.setColor(c);
		
		System.out.println("Por favor, introduzca el número de bastidor: ");
		String nb = leerTeclado.nextLine();
		coche.setNumeroBastidor(nb);
		
		System.out.println("Por favor, introduzca los kilómetros: ");
		int km = leerTeclado.nextInt();
		coche.setKilometros(km);
		
		System.out.println("Por favor, introduzca el año de fabricación: ");
		int af = leerTeclado.nextInt();
		coche.setAñofabricacion(af);
		
		System.out.println("Por favor, introduzca true si es electrico y false si no lo es: ");
		boolean e = leerTeclado.nextBoolean();
		coche.setElectrico(e);
		
		System.out.println( "Vehículo:\n\t" + coche.toString() + " \n");

		
	}*/
	
	public static void Barco (Scanner leerTeclado){
		
		//String marca, String color, String numeroBastidor, int kilometros, int añofabricacion, int eslora,
		//int calado, Tipo tipo
		Barco barco = new Barco(null, null, null, 0, 0, 0, 0, null);;
		
		//String marca, String color, String numeroBastidor, int kilometros, int añofabricacion, boolean electrico
		
		System.out.println("Vamos a introducir los datos del coche. \n");
		System.out.println("Con más tiempo lo acabo...  los conocimientos se ven. Tampoco me ha dado tiempo al try catch. Gracias Sergio y disculpa las molestias \n");
		
		System.out.println("Por favor, introduzca la marca: ");
		String m = leerTeclado.nextLine();
		barco.setMarca(m); //con esto le indicas donde debe rellenar este campo 
		
		System.out.println("Por favor, introduzca el color: ");
		String c = leerTeclado.nextLine();
		barco.setColor(c);
		
		System.out.println("Por favor, introduzca el número de bastidor: ");
		String nb = leerTeclado.nextLine();
		barco.setNumeroBastidor(nb);
		
		System.out.println("Por favor, introduzca los kilómetros: ");
		int km = leerTeclado.nextInt();
		barco.setKilometros(km);
		
		System.out.println("Por favor, introduzca el año de fabricación: ");
		int af = leerTeclado.nextInt();
		barco.setAñofabricacion(af);
		
		/*System.out.println("Por favor, introduzca true si es electrico y false si no lo es: ");
		boolean e = leerTeclado.nextBoolean();
		barco.setElectrico(e);*/
		
		System.out.println( "Vehículo:\n\t" + barco.toString() + " \n");

		
	}
	
	
}
