package examenmario;

import java.util.Calendar;
import java.util.Date;

public class Coche extends Vehiculo{

	
	protected boolean electrico;
	private boolean antiguo;
	
	public Coche(String marca, String color, String numeroBastidor, int kilometros, int aniofabricacion, boolean antiguo, boolean electrico) {
		super(marca, color, numeroBastidor, kilometros, aniofabricacion);
		this.electrico = electrico;
		this.antiguo= antiguo;
	}
	
	public boolean isElectrico() {

		return electrico;
	}
	public void setElectrico(boolean electrico) {
		this.electrico = electrico;
	}
	public boolean isAntiguo() {
		if ( edadCoche() == true) {
			return false; //es moderno
		}else {
			return true; //es antiguo
		}
	}
	public void setAntiguo(boolean antiguo) {
		this.antiguo = antiguo;
	}

	@Override
	public String toString() {
		return "Coche ["+ super.toString() +"electrico=" + electrico + ", antiguo=" + isAntiguo() + "]";
	}
	
	public boolean edadCoche() {
		Calendar fecha = Calendar.getInstance();
		int anio = fecha.get(Calendar.YEAR);
		
		if( anio - aniofabricacion < 25) {
			return true; //es moderno
		}else {
			return false; //es antiguo
		}
	}
	
	
}
